# Awesome VSCode

- [Plugins](#plugins)
- [Syntax](#syntax)
- [Shortcuts](#shortcuts)

### Plugins

* [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager)

This is a super handy tool for switching between various projects, even
if you don't really do it so much. Just Shift+Alt+P and change the
project!

* [Align](https://marketplace.visualstudio.com/items?itemName=steve8708.Align)

Does what it says, and it does it neatly!

### Shortcuts

Here is what I use:

```
[
    {
        "key": "ctrl+shift+t",
        "command": "workbench.action.terminal.new",
        "when": "terminalFocus"
    },
    {
        "key": "ctrl+shift+pageup",
        "command": "workbench.action.terminal.focusPrevious",
        "when": "terminalFocus"
    },
    {
        "key": "ctrl+shift+pagedown",
        "command": "workbench.action.terminal.focusNext",
        "when": "terminalFocus"
    },
    {
        "key": "ctrl+r",
        "command": "workbench.action.gotoSymbol"
    },
    {
        "key": "ctrl+t",
        "command": "workbench.action.quickOpen"
    }
]
```